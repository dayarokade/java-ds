//WAP to take a size of array from user and also take an integer elements from user print sum of odd elements only
// input:enter size:5
 //enter array elements : 1 2 3 4 5
  //o/p:9
  


import java.util.*;

class demo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter array size");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("enter elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int sum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}
			System.out.println("sum of odd element is="+sum);
		}
	}
