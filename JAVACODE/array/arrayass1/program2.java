//print of even elements only
//enter array size:9
//enter array elements: 1 2 3 2 5 10 55 77 99
//o/p:40


import java.util.*;

class demo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter array size");
                int size=sc.nextInt();
                int arr[]=new int[size];
                System.out.println("enter elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                int mul=1;
                for(int i=0;i<arr.length;i++){
                        if(arr[i]%2==0){
                                mul=mul*arr[i];
                        }
                }
                        System.out.println("product of even element is="+mul);
                }
        }

