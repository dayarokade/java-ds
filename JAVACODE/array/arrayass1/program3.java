//print product of odd index only
//enter array size:6
//enter array elements: 1 2 3 4 5 6
//o/p:48




import java.util.*;

class demo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter array size");
                int size=sc.nextInt();
                int arr[]=new int[size];
                System.out.println("enter elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                int mul=1;
                for(int i=0;i<arr.length;i++){
                        if(i%2==1){
                                mul=mul*arr[i];
                        }
                }
                        System.out.println("product of odd index is="+mul);
                }
        }
