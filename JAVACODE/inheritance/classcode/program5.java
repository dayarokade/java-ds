//static in parent block
class parent{
        static{
        
                System.out.println("In parent static block");
        }
}
                class child extends parent{
                      static{
                                System.out.println("In child static block");
                }
                }

                class client{
                        public static void main(String[]args){
                                child obj= new child();
			}
		}
