class parent{
	int x=10;
        static int y=10;
        static{

                System.out.println("parent static block");
          }
	parent(){
		System.out.println("In constructor");
	}
         void methodone(){
         System.out.println(x);
		System.out.println(y);
         }
         static void methodtwo(){
		 System.out.println(y);
	 }
}
                class child extends parent{
                      static{
         System.out.println("In child static block");
		      }
	 child() {
         System.out.println("In child constructor");
        
                }
		}

                class client{
                        public static void main(String[]args){
                                 child obj= new child();
				 obj.methodone();
				 obj.methodtwo();
                        }
                }
