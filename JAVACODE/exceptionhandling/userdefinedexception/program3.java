import java.util.Scanner;

class NumberUnderflowException extends RuntimeException{

	NumberUnderflowException(String msg){
		super(msg);
	}
}

class demo{

	public static void main(String [] p){

		Scanner sc =new Scanner(System.in);

		int arr[]=new int[10];

		System.out.println("enter data of mobile no");
		System.out.println("Note: 0 < number");

		for(int i=0;i<arr.length;i++){

			int data=sc.nextInt();
			if(data < 0)
				throw new NumberUnderflowException("bhava digit 0 peksha lahan aahe");
			
			arr[i]=data;
		}
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+ " ");
		}
	}
}


