import java.util.Scanner;
class DataOverflowException extends RuntimeException{
	DataOverflowException(String msg){
		super(msg);
	}
}
class DataUnderflowException extends RuntimeException{
	DataUnderflowException(String msg){
		super(msg);
	}
}
class demo{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		int arr[]=new int[5];
		System.out.println("enter data");
		System.out.println("Note:0<element<100");
		for(int i=0;i<arr.length;i++){
			int data=sc.nextInt();
			if(data<0)
				throw new DataUnderflowException("bhava data 0 peksha motha aahe");
			if(data>100)
				throw new DataOverflowException("bhava data 100 peksha motha aahe");
			arr[i]=data;
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]+"");
		}
	}
}
