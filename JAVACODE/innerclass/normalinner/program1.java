class outer{
	class inner{
		void fun2(){
			System.out.println("fun2-inner");
		}
	}
	void fun1(){
		System.out.println("fun1-outer");
	}
}
class client {
	public static void main(String[]args){
		outer obj=new outer();
		obj.fun1();
		outer.inner obj1=obj.new inner();
		obj1.fun2();
	}
}

