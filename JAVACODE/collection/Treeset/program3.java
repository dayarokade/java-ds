import java.util.*;
class Employee{
        String empName=null;
        float sal=0.0f;
        Employee(String empName,float sal){
                this.empName=empName;
                this.sal=sal;
       
        }
        public String toString(){
                return "{" + empName + ":" + sal + "}";
        }
}
class sortByName implements Comparator <Employee>{
	public int compare(Employee obj1,Employee obj2){
		return obj1.empName.compareTo(obj2.empName);
	}
}
class sortBysal implements Comparator <Employee>{
	public int compare(Employee obj1,Employee obj2){
		return (int)(obj1.sal-obj2.sal);
	}
}
		class listsortdemo{
        public static void main(String[] args){
                ArrayList <Employee> al=new ArrayList <Employee>();
                al.add(new Employee("kanha",200000.00f));
                al.add(new Employee("Ashish",250000.00f));
                al.add(new Employee("Badhe",150000.00f));
                al.add(new Employee("Rahul",175000.00f));
                        System.out.println(al);
			Collections.sort(al,new sortByName());

                        System.out.println(al);
			Collections.sort(al,new sortBysal());
                        System.out.println(al);
	}
		}
