import java.util.*;
class movies{
        String movieName=null;
	double totcoll=0.0;
        float imdbRating=0.0f;
        movies(String movieName,double totcoll, float imdbRating){
                this.movieName=movieName;
                this.totcoll=totcoll;
		this.imdbRating=imdbRating;

        }
        public String toString(){
                return "{" + movieName + "," + totcoll + "," + imdbRating + "}";
        }
}


class sortByName implements Comparator{

        public int compare(Object obj1,Object obj2){

                return ((movies)obj1).movieName.compareTo(((movies)obj2).movieName);
	}
}

class sortBycoll implements Comparator{
        public int compare(Object obj1,Object obj2){
                return (int) ((((movies)obj1).totcoll)-(((movies)obj2).totcoll));
        }
}
class sortByIMDB implements Comparator{
        public int compare(Object obj1,Object obj2){
                return (int) ((((movies)obj1).imdbRating)-(((movies)obj2).imdbRating));
        }
}
                class userlistsort{
        public static void main(String[] args){
                ArrayList al=new ArrayList();
                al.add(new movies("RHTDM",200.00,8.8f));
                al.add(new movies("VED",75.00,7.5f));
                al.add(new movies("SAIRAT",150.00,6.5f));
                al.add(new movies("BAJRANG",175.00,9.9f));
                        System.out.println(al);
                        Collections.sort(al,new sortByName());
			System.out.println(al);
                        Collections.sort(al,new sortBycoll());
			System.out.println(al);
                        Collections.sort(al,new sortByIMDB());
			System.out.println(al);
	}
		}
