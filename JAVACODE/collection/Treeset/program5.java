import java.util.*;
class shoes  implements Comparable{
        String shoesName=null;
        float totcoll=0.0f;
        shoes(String shoesName,float totcoll){
                this.shoesName=shoesName;
                this.totcoll=totcoll;
        }
        public int compareTo(Object obj){
                return((shoes)obj).shoesName.compareTo(this.shoesName);
        }
        public String toString(){
                return shoesName;
        }
}
class Treesetdemo{
        public static void main(String[] args){
                TreeSet ts=new TreeSet();
                ts.add(new shoes("PUMA",1500.00f));
                ts.add(new shoes("ADIDAS",1200.00f));
                ts.add(new shoes("NIKE",2500.00f));
                ts.add(new shoes("BATA",1000.00f));
                        System.out.println(ts);
        }
}
