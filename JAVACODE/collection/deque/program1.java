import java.util.*;
class dequedemo{
        public static void main(String[] args){
                Deque obj=new ArrayDeque();
                obj.offer(10);
                obj.offer(40);
                obj.offer(20);
                obj.offer(30);
                System.out.println(obj);
		//offerfirst()
		//offerlast()
		obj.offerFirst(5);
		obj.offerLast(50);
		System.out.println(obj);
		//pollfirst()
		////polllast()
		obj.pollFirst();
		obj.pollLast();
		System.out.println(obj);
		//peekfirst()
		//peeklast
		obj.peekFirst();
		obj.peekLast();
		System.out.println(obj);
		//iterator
		Iterator itr=obj.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
			//descendingiterator		
		}
		Iterator itr2=obj.descendingIterator();
		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}
		

        }
}
