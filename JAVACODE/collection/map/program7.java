//methods of hashmap class

import java.util.*;
class demo{
        public static void main(String[] args){
		HashMap hm=new HashMap();
		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");
		System.out.println(hm);
		//get
		System.out.println(hm.get("Python"));
		//keyset
		System.out.println(hm.keySet());
		//values
		System.out.println(hm.values());
		//entryset
		System.out.println(hm.entrySet());
		


        }
}
