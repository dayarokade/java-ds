import java.util.*;
class demo{
        String str;
        demo(String str){
        this.str=str;
}
public String toString(){
        return str;
}
public void finalize(){
        System.out.println("Notify");
}
}
class GCdemo{
        public static void main(String[] args){
		demo obj1=new demo("core2web");
		demo obj2=new demo("biencaps");
		demo obj3=new demo("incubator");
                WeakHashMap hm=new WeakHashMap();
		hm.put(obj1,2016);
		hm.put(obj2,2019);
		hm.put(obj3,2023);
		obj1=null;
		System.gc();
		System.out.println(hm);


        }
}
