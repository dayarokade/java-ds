import java.util.*;
class platform{
        String str=null;
        int foundYear=0;
        platform(String str,int foundYear){
                this.str=str;
                this.foundYear=foundYear;
	}
        public String toString(){
                return "{" + str  + foundYear + "}";
	}
	}
       class SortByName implements Comparator{
	       public int compare(Object obj1,Object obj2){
		       return((platform)obj1).str.compareTo(((platform)obj2).str);
        }
}
class treemapdemo{
        public static void main(String[] args){
                TreeMap tm=new TreeMap(new SortByName());
                tm.put(new platform("Youtube",  2005),"Google");
                tm.put(new platform("Instagram", 2010),"Meta");
                tm.put(new platform("Facebook", 2004),"Meta");
                tm.put(new platform("ChatGPT", 2022),"OpenAI");

                System.out.println(tm);
        
}
}
