import java.util.*;
class platform implements Comparable{
	String str=null;
	int foundYear=0;
	platform(String str,int foundYear){
		this.str=str;
		this.foundYear=foundYear;
	}
	public String toString(){
		return "{" + str  + foundYear + "}";
	}
	public int compareTo(Object obj){
		return this.foundYear-((platform)obj).foundYear;
	}
}
class treemapdemo{
        public static void main(String[] args){
                TreeMap tm=new TreeMap();
                tm.put(new platform("Youtube",  2005),"Google");
                tm.put(new platform("Instagram", 2010),"Meta");
                tm.put(new platform("Facebook", 2004),"Meta");
                tm.put(new platform("ChatGPT", 2022),"OpenAI");
             
                System.out.println(tm);
        }
}
