import java.util.*;
class demo{
	String str;
	demo(String str){
	this.str=str;
}
public String toString(){
	return str;
}
public void finalize(){
	System.out.println("Notify");
}
}
class GCdemo{
        public static void main(String[] args){
                demo obj1=new demo("core2web");
                demo obj2=new demo("biencaps");
                demo obj3=new demo("incubator");

                System.out.println(obj1);
                System.out.println(obj2);
                System.out.println(obj3);
		obj1=null;
		obj2=null;
		System.gc();
		System.out.println("In main");


        }
}
