//sorted map using methods


import java.util.*;
class sortedmapdemo{
        public static void main(String[] args){
                SortedMap tm=new TreeMap();
                tm.put("Ind","India");
		tm.put("Pak","pakistan");
		tm.put("SL","srilanka");
		tm.put("Aus","australia");
		tm.put("Ban","bangladesh");
                System.out.println(tm);
	//submap       
       System.out.println(tm.subMap("Aus","Pak"));
       //headmap
       System.out.println(tm.headMap("Pak"));
       //tailmap
       System.out.println(tm.tailMap("Pak"));
       //firstkey
       System.out.println(tm.firstKey());
       //lastkey
       System.out.println(tm.lastKey());
       //keyset
       System.out.println(tm.keySet());
       //values
       System.out.println(tm.values());
       //entryset
       System.out.println(tm.entrySet());


	}


}
