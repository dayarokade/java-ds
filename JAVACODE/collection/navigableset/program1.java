import java.util.*;
class navigablesetdemo{
	public static void main(String[] args){
		NavigableSet ns=new TreeSet();
		ns.add(10);
		ns.add(30);
		ns.add(14);
		ns.add(27);
		ns.add(23);
		System.out.println(ns);
		//lower
		System.out.println(ns.lower(23));
		//floor
		System.out.println(ns.floor(23));
		//ceiling
		System.out.println(ns.ceiling(23));
		//higher
		System.out.println(ns.higher(27));
		//pollfirst
		System.out.println(ns.pollFirst());
		//polllast
		System.out.println(ns.pollLast());
		System.out.println(ns);
		//descendingset
	System.out.println(ns.descendingSet());
	//iterator
	Iterator itr=ns.iterator();
	while(itr.hasNext()){
		System.out.println(itr.next());
	}
	//descendingiterator
	Iterator itr1=ns.descendingIterator();
	while(itr1.hasNext()){
		System.out.println(itr1.next());
	}
	//subset
		System.out.println(ns.subSet(10,true,27,false));
	}

}
