interface demo1{
	void fun();
}
interface demo2{

	void fun();
}
class demochild1 implements demo1,demo2{

	public void fun(){
		System.out.println("in child fun");
	}
}

class client{

	public static void main(String [] args){

		demo1 obj=new demochild1();
		obj.fun();
		
		demo2 obj1=new demochild1();
		obj1.fun();
		
	}
}



