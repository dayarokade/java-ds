interface demo{
	void fun();
	void gun();
}
abstract class demochild implements demo{

	public void gun(){

		System.out.println("in gun");
	}
}
class demochild1 extends demochild{
	public void fun(){
		System.out.println("in fun");
	}
}

class client{

	public static void main(String [] p){

		demo obj=new demochild1();
		obj.fun();
		obj.gun();
	}
}



