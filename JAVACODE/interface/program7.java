interface demo{
	default void fun(){
		System.out.println("in fun demo");
	}
}
interface demo2{
	default void fun(){
		System.out.println("in fun demo2");
	}
}
class demochild implements demo,demo2{
	
       public void fun(){
	       System.out.println("in fun demochild");
       }
}
class client{
	public static void main(String[]args){
		demochild obj=new demochild();
		obj.fun();
	
	}
}
