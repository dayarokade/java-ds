        class parent{
                parent(){
                System.out.println("In parent constructor");
        }
        void fun(int x){
                System.out.println("In parent fun");
       }
        }
       class child extends parent{
               child(){
                       System.out.println("child constructor");
               }
               void fun(){
                       System.out.println("In child fun");
               }
       }
       class client{
       public static void main(String[]args){
               parent obj1=new child();
               obj1.fun();			//error

       }
}
