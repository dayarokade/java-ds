class parent{
	char fun(){
		System.out.println("parent fun");
		return 'A';
	}
}
class child extends parent{
	int fun(){
		System.out.println("child fun");
		return 1;
	}
}
public static void main (String[] args){
	parent obj = new child();
	obj.fun();
}
}
