class parent{
	parent(){
		System.out.println("parent constructor");
	}
	void fun(int x){
		System.out.println("in parent fun");
	}
}
class child extends parent{

	child(){
		System.out.println("child contructor");
	}
	void fun(int x){

		System.out.println("in child fun");
	}
}
class client{
	public static void main(String []args){

		parent obj=new child();
		obj.fun(10); 
	
	}
}

